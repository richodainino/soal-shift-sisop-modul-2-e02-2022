#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "mkdir /home/haniif/shift2", NULL);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }

  child_id = fork();

  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "mkdir /home/haniif/shift2/drakor", NULL);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }

  child_id = fork();

  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "unzip drakor.zip -d /home/haniif/shift2/drakor", NULL);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }

  child_id = fork();
  
  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; rm -r `find . ! -iname \"*.png\"`", NULL);
  } else {
    // this is parent              
    while ((wait(&status)) > 0);
  }

  child_id = fork();

  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; mkdir `ls | sort | awk -F[._\\;] '{for(num=3;num<NF;num=num+3) print $num;}'`", NULL);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }

  child_id = fork();

  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; ls | sort | awk -F[._\\;] '{for(num=3;num<NF;num=num+3) {var=\"echo \\\"kategori : \"$num\"\\\" > \"$num\"/data.txt\"; system(var);}}'", NULL);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }

  child_id = fork();

  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; ls | sort | awk -F[._\\;] -v q=\"'\" '{for(num=3;num<NF;num=num+3) {var=\"cp \"q $0 q \" \" $num\"/\"$(num-2)\".png\";  system(var);}}'",NULL);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }

  child_id = fork();

  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; ls | sort | awk -F[._\\;] '{for(num=3;num<NF;num=num+3) print $(num-1)\"_\"$(num-2)\"_\"$num}' | sort | awk -F[_] '{var=\"echo \\\"\\nnama : \"$2\"\\nrilis : tahun \"$1\"\\\" >> \"$3\"/data.txt\"; system(var);}'", NULL);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }

  child_id = fork();

  if (child_id == 0) {
    // this is child
    execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; rm -r `find . -maxdepth 1 -iname \"*.png\"`", NULL);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }

}
