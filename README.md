# Sisop soal shift modul 2 kelompok E02 2022
Anggota:
1. Haniif Ahmad Jauhari | 5025201224
2. Made Rianja Richo Dainino | 5025201236
3. Dicky Indra Kuncahyo | 5025201250

## Soal 1
Dikerjakan oleh Dicky Indra Kuncahyo | 5025201250


## Soal 2
Dikerjakan oleh Haniif Ahmad Jauhari | 5025201224

Pada soal 2 ini, kita gunakan fungsi sejenis `exec`. Agar program dapat menjalankan beberapa fungsi `exec`, maka di dalam program harus ada *forking* agar child program saja yang menjalankan fungsi `exec` sehingga main program dapat terus berjalan.

    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
       exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "mkdir /home/haniif/shift2", NULL);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }

Potongan kode diatas digunakan untuk melakukan *forking* dan memberi perintah `exec` kepada child program untuk membuat folder `shift2` pada path `/home/haniif/shift2`

    child_id = fork();

    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "mkdir /home/haniif/shift2/drakor", NULL);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }

Potongan kode tersebut berfungsi untuk melakukan *forking* dan menmberi perintah `exec` kepada child program untuk membentuk folder
`drakor` pada path `/home/haniif/shift2/drakor`. Untuk menjalankan perintah `exec` selanjutnya, tetap digunakan pola yang sama yakni melakukan *forking* lalu memberi perintah `exec` kepada child program sehingga main program dapat terus berjalan.

    child_id = fork();

    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "unzip drakor.zip -d /home/haniif/shift2/drakor", NULL);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }

Perintah `exec` di atas digunakan untuk melakukan `unzip` file `drakor.zip` pada path `/home/haniif/shift2/drakor`.

    child_id = fork();
  
    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; rm -r `find . ! -iname \"*.png\"`", NULL);
    } else {
        // this is parent              
        while ((wait(&status)) > 0);
    }

Kemudaian, perintah `exec` di atas digunakan untuk menghapus semua file yang tidak berakhiran `.png` pada path `/home/haniif/shift2/drakor`.

    child_id = fork();

    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; mkdir `ls | sort | awk -F[._\\;] '{for(num=3;num<NF;num=num+3) print $num;}'`", NULL);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }

Perintah `exec` di atas digunakan untuk membuat folder `kategori` pada path `/home/haniif/shift2/drakor`.

    child_id = fork();

    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; ls | sort | awk -F[._\\;] '{for(num=3;num<NF;num=num+3) {var=\"echo \\\"kategori : \"$num\"\\\" > \"$num\"/data.txt\"; system(var);}}'", NULL);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }

Perintah `exec` di atas digunakan untuk membuat file `data.txt` pada setiap folder `kategori` dan mengisinya dengan baris `kategori : <kategori>`.

    child_id = fork();

    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; ls | sort | awk -F[._\\;] -v q=\"'\" '{for(num=3;num<NF;num=num+3) {var=\"cp \"q $0 q \" \" $num\"/\"$(num-2)\".png\";  system(var);}}'",NULL);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }

Perintah `exec` di atas digunakan untuk melakukan copy file gambar poster ke dalam folder `kategori` masing-masing poster dengan format penamaan `<title>.png`.

    child_id = fork();

    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; ls | sort | awk -F[._\\;] '{for(num=3;num<NF;num=num+3) print $(num-1)\"_\"$(num-2)\"_\"$num}' | sort | awk -F[_] '{var=\"echo \\\"\\nnama : \"$2\"\\nrilis : tahun \"$1\"\\\" >> \"$3\"/data.txt\"; system(var);}'", NULL);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }

Perintah `exec` di atas digunakan untuk menuliskan nama dan tahun rilis setiap poster pada file `data.txt` yang sesuai dengan kategori poster dengan format berikut.
>nama : \<title><br>
rilis : \<tahun rilis>

    child_id = fork();

    if (child_id == 0) {
        // this is child
        execl("/bin/sh", "/bin/sh", "-c", "cd /home/haniif/shift2/drakor; rm -r `find . -maxdepth 1 -iname \"*.png\"`", NULL);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }

Potongan kode terakhir di atas digunakan untuk memberikan perintah `exec` pada child program yang berfungsi untuk menghapus file `.png` yang berada di luar folder `kategori` tetapi masih di dalam folder `drakor`.


## Soal 3
Dikerjakan oleh Made Rianja Richo Dainino | 5025201236

Pada soal 3.a kita diminta untuk membuat 2 directory dengan jeda 3 detik di antara pembuatannya. Kita dapat menyelesaikannya dengan `fork` lalu melakukan `exec mkdir`, lalu untuk jeda 3 detik yang diminta kita dapat menggunakan fungsi `sleep(3)`.
```c
if (child_id == 0)
{ // Make darat dir
    char *argv[] = {"mkdir", "-p", "/home/richo/modul2/darat", NULL};
    execv("/bin/mkdir", argv);
}
else
{ 
    while ((wait(&status)) > 0);
    child_id = fork();

    if (child_id == 0)
    { // After 3 second make air dir
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/richo/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
}
```
![](/img/3-1.jpg)

Pada soal 3.b kita diminta untuk mengunzip file zip dari soal yang sudah disediakan. Dapat dilakukan dengan `exec unzip` sebagai berikut.
```c
while ((wait(&status)) > 0);
child_id = fork();

if (child_id == 0)
{ // Unzip
    while ((wait(&status)) > 0);
    char *argv[] = {"unzip", "/home/richo/modul2/animal.zip", "-d", "/home/richo/modul2", NULL};
    execv("/bin/unzip", argv);
}
```

Pada soal 3.c kita diminta untuk memilah file yang sudah di-extract ke dalam masing masing folder yang diharapkan. Pada awalnya saya mengalami kesulitan dimana saya menggunkana exec yang berulang ulang, namun nyatanya ini tidak dapat dilakukan dikarenakan kita tidak mengetahui isi dari directory, sehingga setelah saya teringat dengan materi tambahan pada modul yakni dengan menggunakan directory listing dan menggunakan file handling pada C sebagai berikut.
```c
while ((wait(&status)) > 0);
child_id = fork();

if (child_id == 0)
{ // Move each file
    while ((wait(&status)) > 0);
    char path[100] = "/home/richo/modul2/animal/";
    dp = opendir(path);
    if (dp != NULL)
    {
        while (ep = readdir(dp))
        {
            if (strstr(ep->d_name, "darat"))
            {
                char oriFilePath[100] = "/home/richo/modul2/animal/";
                char newFilePath[100] = "/home/richo/modul2/darat/";
                strcat(oriFilePath, ep->d_name);
                strcat(newFilePath, ep->d_name);
                
                FILE *oriFile, *newFile;
                oriFile = fopen(oriFilePath, "rb");
                newFile = fopen(newFilePath, "ab");

                while(1) {
                    int c = fgetc(oriFile);
                    if (feof(oriFile)) break;
                    fputc(c, newFile);
                }

                fclose(oriFile);
                fclose(newFile);
                remove(oriFilePath);
            }
            else if (strstr(ep->d_name, "air"))
            {
                char oriFilePath[100] = "/home/richo/modul2/animal/";
                char newFilePath[100] = "/home/richo/modul2/air/";
                strcat(oriFilePath, ep->d_name);
                strcat(newFilePath, ep->d_name);
                
                FILE *oriFile, *newFile;
                oriFile = fopen(oriFilePath, "rb");
                newFile = fopen(newFilePath, "ab");

                while(1) {
                    int c = fgetc(oriFile);
                    if (feof(oriFile)) break;
                    fputc(c, newFile);
                }

                fclose(oriFile);
                fclose(newFile);
                remove(oriFilePath);
            }
            else
            {
                char filePath[100] = "/home/richo/modul2/animal/";
                strcat(filePath, ep->d_name);
                remove(filePath);
            }
        }
    }
    char *argv[] = {"rm", "-d", "/home/richo/modul2/animal", NULL};
    execv("/bin/rm", argv);
}
```

Pada soal 3.d kita diminta untuk menghapuskan hewan burung pada directory darat. Saya menggunakan cara yang tidak jauh berbeda dengan cara saya memilah file, namun perbedaannya dimana sebelumnya saya mengcopy binary filenya dengan ibarat meng-copy, disini saya langsung mendelete file "bird" tersebut.
```c
while ((wait(&status)) > 0);
child_id = fork();

if (child_id == 0)
{ // Deleting bird
    while ((wait(&status)) > 0);
    dp = opendir("/home/richo/modul2/darat/");
    if (dp != NULL)
    {
        while (ep = readdir(dp))
        {
            if (strstr(ep->d_name, "bird"))
            {
                char filePath[100] = "/home/richo/modul2/darat/";
                strcat(filePath, ep->d_name);
                remove(filePath);
            }
        }
    }
}
```
![](/img/3-2.jpg)

Pada soal 3.e kita diminta untuk membuat file list.txt pada directory air berisi dengan list semua hewan dengan format `UID_UID file premission_nama file.jpg`. Saya melakukan directory listing untuk dan menggunakan materi tambahan pada modul yakni melihat UID dan UID file permission menggunakan stat.
```c
while ((wait(&status)) > 0);
FILE *listFile;
listFile = fopen("/home/richo/modul2/air/list.txt", "w+");

dp = opendir("/home/richo/modul2/air/");
if (dp != NULL)
{

    while (ep = readdir(dp))
    {
        if (strstr(ep->d_name, "air")) {
            char fileNameFormatted[100] = "";
            struct stat info;
            int r;
            
            char oriFileName[100] = "";
            strcat(oriFileName, ep->d_name);

            char filePath[100] = "/home/richo/modul2/air/";
            strcat(filePath, oriFileName);
            
            r = stat(filePath, &info);
            if( r==-1 )
            {
                fprintf(stderr,"File error\n");
                exit(1);
            }

            struct passwd *pw = getpwuid(info.st_uid);

            char user[20] = "";
            if (pw != 0) strcat(user, pw->pw_name);
            strcat(user, "_");

            char permission[5] = "";
            if( info.st_mode & S_IRUSR )
                strcat(permission, "r");
            if( info.st_mode & S_IWUSR )
                strcat(permission, "w");
            if( info.st_mode & S_IXUSR )
                strcat(permission, "x");
            strcat(permission, "_");
            
            strcat(fileNameFormatted, user);
            strcat(fileNameFormatted, permission);
            strcat(fileNameFormatted, oriFileName);
            fprintf(listFile, "%s\n", fileNameFormatted);
        }
    }
}

fclose(listFile);
```
![](/img/3-3.jpg)
![](/img/3-4.jpg)
